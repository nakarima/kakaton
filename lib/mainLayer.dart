import 'package:flutter/material.dart';
import 'package:hackaton/Event.dart';
import 'package:hackaton/HomePage.dart';
import 'package:hackaton/observed.dart';
import 'package:hackaton/recommended.dart';
import 'package:hackaton/AppState.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';

typedef RefreshDelegate = Future Function();

class MainLayer extends StatefulWidget {
  MainLayer({Key key, this.recommendedEvents}) : super(key: key);
  final  List<Event> recommendedEvents;
  @override
  _MainLayerState createState() => new _MainLayerState();
}

class _MainLayerState extends State<MainLayer> {


  int _page = 0;
  PageController _pageController;
  List<Event> observedEvents = new List();


  @override
  void initState() {
    super.initState();
    _pageController = new PageController();

  }
  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.grey[900],
        title: new Text(
          "Eventify",
          style: new TextStyle(color: Colors.greenAccent),
        ),
      ),
      body: new PageView(
        children: <Widget>[
          new HomePage(observedEvents: observedEvents, recommendedEvents: widget.recommendedEvents,),
          new ObservedScroll(widget.recommendedEvents, observedEvents),
          new RecommendedScroll(widget.recommendedEvents, observedEvents, )
        ],
        onPageChanged: onPageChanged,
        controller: _pageController,
      ),
      bottomNavigationBar: new Material (
      color: Colors.grey[900],
      child:new BottomNavigationBar(

        fixedColor: Colors.greenAccent,
        currentIndex: _page,
        onTap: navigationTapped,
        items: [
          new BottomNavigationBarItem(
            icon: new Icon(Icons.home,color:(_page != 0 ?  Colors.white : Colors.greenAccent)),
            backgroundColor: Colors.grey[900],
            title: new Text('Strona główna', style: new TextStyle(color: Colors.white),),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.favorite, color:(_page != 1 ?  Colors.white : Colors.greenAccent)),
            backgroundColor: Colors.grey[900],
            title: new Text('Obserwowane', style: new TextStyle(color: Colors.white),),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.thumb_up, color:(_page != 2 ?  Colors.white : Colors.greenAccent)),
            backgroundColor: Colors.grey[900],
            title: new Text('Proponowane', style: new TextStyle(color: Colors.white),),
          ),
        ],
      ),)
    );
  }

  navigationTapped(int currentIndex) {
    _pageController.animateToPage(currentIndex,
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  onPageChanged(int page) {
    setState(() {
      _page = page;
    });
  }

}