import 'package:flutter/material.dart';
import 'package:hackaton/Event.dart';
import 'package:hackaton/EventCard.dart';

class RecommendedScroll extends StatelessWidget {
  final List<Event> recommendedEvents;
  List<Event> subscribedEvents;

  RecommendedScroll(this.recommendedEvents, this.subscribedEvents);

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
      itemCount: recommendedEvents.length,
        itemBuilder: (context, index) {
        return EventCard(recommendedEvents[index], subscribedEvents, recommendedEvents);
        }
    );
  }


}