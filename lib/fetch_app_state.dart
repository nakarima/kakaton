import 'AppState.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart';
import 'Event.dart';

Future<AppState> fetchAppState() async {
  print("dupa4");
  const url = 'http://156.17.141.98:5000/events';
  final response = await get(url);

  if(response.statusCode == 200) {
    print("${json.decode(response.body)}");
    var r = _fromJson(json.decode(response.body));

    return r;
  }
  print("dupa2");
  throw new Exception("Service unavailable");
}

AppState _fromJson(Map<String, dynamic> data) {
  return AppState(

      recommendedEvents: (data['events'] as List).map((t) {
        return Event(
            date: DateTime.parse(t["date"]),
            title: t["name"],
            link: t["url"],
            imgUrl: t["picture_url"],
            subscribed: false,
        );
      }).toList()
  );
}