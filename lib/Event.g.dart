// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Event _$EventFromJson(Map<String, dynamic> json) {
  return Event(
      date:
          json['date'] == null ? null : DateTime.parse(json['date'] as String),
      title: json['title'] as String,
      link: json['url'] as String,
      imgUrl: json['picture_url'] as String,
      );
}

Map<String, dynamic> _$EventToJson(Event instance) => <String, dynamic>{
      'date': instance.date?.toIso8601String(),
      'title': instance.title,
      'link': instance.link,
      'subscribed': instance.subscribed,
      'imgUrl': instance.imgUrl
    };
