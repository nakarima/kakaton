import 'package:json_annotation/json_annotation.dart';
part 'Event.g.dart';

@JsonSerializable()
class Event {
  final DateTime date;
  final String title;
  final String link;
  bool subscribed = false;
  final String imgUrl;

//category,

  Event({this.date, this.title, this.link, this.imgUrl, this.subscribed});


  factory Event.fromJson(Map<String, dynamic> json){
    return Event(
      date:
      json['date'] == null ? null : DateTime.parse(json['date'] as String),
      title: json['title'] as String,
      link: json['url'] as String,
      imgUrl: json['picture_url'] as String,
      subscribed: false
    );
  }
}