import 'package:flutter/material.dart';
import 'package:hackaton/Event.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';


class EventCard extends StatelessWidget {
  final Event event;
  final List<Event> observedEvents;
  final List<Event> recommendedEvents;

  EventCard(this.event, this.observedEvents, this.recommendedEvents);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: EdgeInsets.only(bottom: 0.0),
      child: new Card(
        color: Colors.greenAccent,
        margin: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new SizedBox(
                  height: 151.0,
                  width: 391.42857,
                  child: new ClipRRect(
                    borderRadius: new BorderRadius.vertical(
                        top: Radius.circular(4.0),
                        bottom: Radius.circular(18.0)),
                    child: new Image.network(
                      event.imgUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            ),
            new Padding(
              padding: EdgeInsets.only(top: 2.0,
                  right: 10.0,
                  left: 10.0),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new Expanded(
                    child: new SizedBox(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Row(
                            children: <Widget>[
                              new Padding(
                                padding: EdgeInsets.only(left: 10.0),
                                child: new Text(
                                  "${event.date.day}",
                                  textAlign: TextAlign.center,
                                  textScaleFactor: 1.3,
                                ),
                              )
                            ],
                          ),
                          new Row(
                            children: <Widget>[
                              new Text(
                                "${DateFormat.MMM().format(event.date).toUpperCase()}",
                                textScaleFactor: 1.3,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Text(
                            event.title,
                            textScaleFactor: 1.6,
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      new Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new Text(
                            "${DateFormat.Hm().format(event.date)}",
                            textScaleFactor: 1.0,
                            textAlign: TextAlign.center,
                            style: new TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  new Expanded(
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        new ButtonTheme.bar(
                          child: ButtonBar(
                            children: <Widget>[
                              new RaisedButton(
                                  onPressed: () => launch(event.link),
                                  child: new Text(
                                    "more",
                                    style: new TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.greenAccent),
                                  )),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            StatefulBuilder(builder: (BuildContext context, StateSetter setState){
            return new Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                new SizedBox(
                  width: 391.42857,
                  child: new ClipRRect(
                    borderRadius:
                        new BorderRadius.vertical(bottom: Radius.circular(4.0)),
                    child: new Container(
                      color: Colors.black,
                      child: new FlatButton(
                          onPressed: () {
                            setState(() {
                              if(event.subscribed) {
                                observedEvents.remove(event);
                                event.subscribed = false;
                              }
                              else {
                                event.subscribed = true;
                                observedEvents.add(event);
                              }
                            });
                          }, child: new Text(event.subscribed ? "Przestań obserwować" : "Zaobserwuj",  style: new TextStyle(
                          fontWeight: FontWeight.bold,
                          color: event.subscribed ? Colors.redAccent :Colors.greenAccent),)),
                    ),
                  ),
                )
              ],
            );}),
          ],
        ),
      ),
    );
  }

}
