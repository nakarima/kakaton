import 'package:flutter/material.dart';
import 'package:hackaton/Event.dart';
import 'package:hackaton/EventCard.dart';

class ObservedScroll extends StatelessWidget {
  final List<Event> recommendedEvents;
  final List<Event> subscribedEvents;

  ObservedScroll(this.recommendedEvents, this.subscribedEvents);

  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemCount: subscribedEvents.length,
        itemBuilder: (context, index) {
           if(subscribedEvents.length != 0) { return EventCard(subscribedEvents[index], subscribedEvents, recommendedEvents); }
        }
    );
  }


}