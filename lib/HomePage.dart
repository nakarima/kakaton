import 'package:flutter/material.dart';
import 'package:hackaton/Event.dart';
import 'package:hackaton/EventCard.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.observedEvents, this.recommendedEvents}) : super(key: key);
  final List<Event> observedEvents;
  final List<Event> recommendedEvents;


  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {




  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      body: Container(

        // Add box decoration
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            // Add one stop for each color. Stops should increase from 0 to 1
            stops: [0.1, 0.4, 0.7, 0.9],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Colors.grey[800],
              Colors.grey[800],
              Colors.grey[800],
              Colors.grey[800],


            ],
          ),
        ),
       child: new ListView(

          padding: const EdgeInsets.only(bottom: 10.0),
          children: <Widget>[
        new Column(
          crossAxisAlignment: CrossAxisAlignment.start,

          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(top: 5.0, left: 10.0),
              child: Text(
                "Najbliższe obserwowane wydarzenie:",
                textAlign: TextAlign.left,
                textScaleFactor: 1.5,
                style: new TextStyle(fontWeight: FontWeight.bold, color: Colors.greenAccent),
              ),
            ),
            widget.observedEvents.length != 0 ? new EventCard(widget.observedEvents.first, widget.observedEvents, widget.recommendedEvents) : new Row( mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[ new Text("Brak obserwowanych wydarzeń", textAlign: TextAlign.center, style: new TextStyle(color: Colors.white),),]),
            new Padding(
              padding: EdgeInsets.only(top: 0.0, left: 10.0),
              child: Text(
                "Najbliższe proponowane wydarzenie:",
                textAlign: TextAlign.left,
                textScaleFactor: 1.5,
                style: new TextStyle(fontWeight: FontWeight.bold, color: Colors.greenAccent),
              ),
            ),
           widget.recommendedEvents != null ? new EventCard(widget.recommendedEvents.first, widget.observedEvents, widget.recommendedEvents) : new Text("nic nie obserwujesz"),

          ],
        ),
      ],),),
    );
  }


  }

