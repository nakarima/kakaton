import 'package:flutter/material.dart';
import 'package:hackaton/root.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Flutter Demo',
      theme: new ThemeData(

        canvasColor: Colors.grey[900],
        buttonColor: Colors.grey[900],
        primarySwatch: Colors.blue,
        iconTheme: new IconThemeData(color:Colors.white,)
      ),
      home: new RootWidget(),
    );
  }
}

